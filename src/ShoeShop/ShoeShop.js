import React, { Component } from 'react'
import Cart from './Cart';
import { dataShoe } from './DataShoe'
import ItemShoe from './ItemShoe';
import ListShoe from './ListShoe';

export default class ShoeShop extends Component {
  state = {
    listShoe: dataShoe,
    cart: [],
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index==-1){
      let newShoe = {...shoe, soLuong:1};
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    })
  }

  handleChangeQuantity = (id, soLuong) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });

    cloneCart[index].soLuong +=soLuong;
    if(cloneCart[index].soLuong== 0){
      cloneCart.splice(index,1);
    }
    this.setState({
      cart: cloneCart,
    });
  };

  handleRemoveShoe = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    cloneCart.splice(index,1);
    this.setState({
      cart: cloneCart,
    })
  };


  render() {
    // console.log("props", this.props);
    return (
      <div className='container'>
        <h2>{this.props.children}</h2>
        <Cart 
          handleChangeQuantity = {this.handleChangeQuantity}
          handleRemoveShoe = {this.handleRemoveShoe}
          cart={this.state.cart}/>
        <ListShoe 
          handleAddToCart = {this.handleAddToCart}
          
          list = {this.state.listShoe}
        />
      </div>
    )
  }
}
