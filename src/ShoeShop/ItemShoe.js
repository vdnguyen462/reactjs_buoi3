import React, { Component } from 'react'

export default class ItemShoe extends Component {
  render() {
    let {image, name, price} = this.props.item;
    return (
      <div className='col-3 p-4'>
        <div className="card" style={{width: '16rem', height: '27rem'}}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{price}</p>
            <a 
            onClick={() => {
              this.props.handleOnClick(this.props.item)
            }}
            href="#" className="btn btn-primary">Add to cart</a>
        </div>
        </div>
      </div>
    )
  }
}
