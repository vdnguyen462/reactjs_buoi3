import React, { Component } from 'react'
import Cart from './Cart';
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {

    handleListShoe = () => {
        let listShoe = this.props.list.map((shoe) => {
            return <
              ItemShoe 
              handleOnClick = {this.props.handleAddToCart}
              item = {shoe}/>
        });
        return listShoe;
    };

  render() {
    return (
      <div>
        <div className='row'>{this.handleListShoe()}</div>  
      </div>
    )
  }
}
