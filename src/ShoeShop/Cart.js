import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <button onClick={() => {
                            this.props.handleChangeQuantity(item.id, -1);
                        }}
                        className='btn btn-danger'>-</button>
                        <span className='mx-3'>{item.soLuong}</span>
                        <button onClick={() => {
                            this.props.handleChangeQuantity(item.id, 1);
                        }}
                        className='btn btn-success'>+</button>
                    </td>
                    <td>{item.price*item.soLuong}</td>
                    <td>
                        <img src={item.image} style={{width: 80}} />
                    </td>
                    <td>
                        <button 
                        onClick={() => {
                            this.props.handleRemoveShoe(item)
                        }}
                        className='btn btn-danger'>Xoá</button>
                    </td>
                </tr>
            );
        });
    };
  render() {
    return (
      <div>
        <table className="table">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Quality</th>
                <th>Price</th>
                <th>Img</th>
                <th>Action</th>
            </thead>
            <tbody>
                {this.renderTbody()}
            </tbody>
        </table>
      </div>
    )
  }
}
