import logo from './logo.svg';
import './App.css';
import ShoeShop from './ShoeShop/ShoeShop';

function App() {
  return (
    <div className="App">
      <ShoeShop>Cyber Shoe Shop</ShoeShop>
    </div>
  );
}

export default App;
